---
share: true
aliases: [Entwurfsmuster]
tags: [cleancode/designpatterns, cleancode/refactoring]
title: Design Patterns
---
# Design Patterns
**Design patterns** are typical solutions to commonly occurring problems in software design. They are like pre-made blueprints that you can customize to solve a recurring design problem in your code
## Creational
These patterns provide various object creation mechanisms, which increase flexibility and reuse of existing code.
- [[Factory]]
- [[Abstract Factory]]
- [[Builder]]
- [[Prototype]]
- [[Singleton]]
## Structural
These patterns explain how to assemble objects and classes into larger structures while keeping these structures flexible and efficient.
- [[Adapter]]
- [[Bridge]]
- [[Composite]]
- [[Decorator]]
- [[Facade]]
- [[Flyweight]]
- [[Proxy]]
## Behavioral
These patterns are concerned with algorithms and the assignment of responsibilities between objects.
- [[Chain of Responsibility]]
- [[Command]]
- [[Iterator]]
- [[Mediator]]
- [[Memento]]
- [[Observer]]
- [[State]]
- [[Strategy]]
- [[Template Method]]
- [[Visitor]]

## Source
The initial Version of the listed Design Pattern pages will be a copy of [Refactoring Guru](https://refactoring.guru/design-patterns)