---
unfinished: true
share:
---
Normal synchron:

```csharp
public void Test(){
	Method();
	Method2();
}
```

Asynchron:

```csharp
public async Task TestAsync() 
{
	await Method1Async();
	this.Method2();
}
```

Erst Methode 1 fertig ausführen dann zwei  (intuitiv)

Macht im Prinzip:

```csharp
public async Task TestAsync() 
{
	Method1Async().ContinueWith( (state) => {
		this.Method2();
	});
}
```


## Was ist Task?
Repräsentiert eine asynchrone Operation welche irgendwann fertig ist.

Ist also immer das Ergebnis einer asynchronen Methode.

Intern generiert die Sprache dafür eine Statemachine welche prüft ob die operation schon abgeschlossen ist.

Was passiert bei async void:
Statemachine wird nicht zurückgegeben -> Caller hat keine Ahnung wann es fertig ist. Irgendwann Exception weil nicht um Task gekümmert.
## Compiler Transformation Example

Let's start with a simple async method:

```csharp
public async Task<int> MyAsyncMethod(int delay) 
{ 
	Console.WriteLine("Starting"); 
	await Task.Delay(delay);
	Console.WriteLine("Finished");
	return 42;
}
```

The compiler transforms this method into a state machine. Here's a simplified version of what the compiler generates:

```csharp
[CompilerGenerated]
private sealed class <MyAsyncMethod> d__1 : IAsyncStateMachine 
{
    public int <>1__state;
    public AsyncTaskMethodBuilder<int> <>t__builder;
    public int delay;
    private TaskAwaiter<> u__1;

    void IAsyncStateMachine.MoveNext()
    {
        int num1 = this.<>1__state;
        int result;
        try
        {
            TaskAwaiter awaiter;
            if (num1 != 0)
            {
                Console.WriteLine("Starting");
                awaiter = Task.Delay(this.delay).GetAwaiter();
                if (!awaiter.IsCompleted)
                {
                    this.<>1__state = 0;
                    this.<>u__1 = awaiter;
                    this.<>t__builder.AwaitUnsafeOnCompleted(ref awaiter, ref this);
                    return;
                }
            }
            else
            {
                awaiter = this.<>u__1;
                this.<>u__1 = new TaskAwaiter();
                this.<>1__state = -1;
            }

            awaiter.GetResult();
            Console.WriteLine("Finished");
            result = 42;
        }
        catch (Exception ex)
        {
            this.<>1__state = -2;
            this.<>t__builder.SetException(ex);
            return;
        }

        this.<>1__state = -2;
        this.<>t__builder.SetResult(result);
    }
}
```
## Explanation of the Transformation

1. **State Machine**: The compiler creates a state machine struct (`<MyAsyncMethod>d__1`) that implements `IAsyncStateMachine`.
2. **State Tracking**: The `<>1__state` field keeps track of the current state of the method execution.
3. **Task Builder**: The `<>t__builder` field is an `AsyncTaskMethodBuilder<int>` that handles the creation and completion of the `Task<int>` that the method returns.
4. **Awaiter Storage**: The `<>u__1` field stores the `TaskAwaiter` for the `await` operation.
5. **MoveNext Method**: This method contains the logic of the original async method, broken down into states.

## How It Works

1. When the method starts, `<>1__state` is -1.
2. It prints "Starting" and starts the delay.
3. If the delay is not completed immediately, it sets the state to 0 and returns, allowing the calling thread to continue execution.
4. When the delay completes, `MoveNext` is called again with `<>1__state` set to 0.
5. It then prints "Finished" and sets the result to 42.
6. Finally, it completes the task with the result.

## Original Method Transformation

The original `MyAsyncMethod` is transformed to something like this:

```csharp
[AsyncStateMachine(typeof(<MyAsyncMethod>d__1))]
public Task<int> MyAsyncMethod(int delay)
{ 
    <MyAsyncMethod > d__1 stateMachine = new <MyAsyncMethod>d__1();
    stateMachine.<>t__builder = AsyncTaskMethodBuilder<int>.Create();
    stateMachine.delay = delay;
    stateMachine.<>1__state = -1;
    stateMachine.<>t__builder.Start(ref stateMachine);
    return stateMachine.<>t__builder.Task;
}
```
This method creates the state machine, initializes it, and starts its execution[1](https://vkontech.com/exploring-the-async-await-state-machine-main-workflow-and-state-transitions/)[2](https://weblogs.asp.net/dixin/understanding-c-sharp-async-await-1-compilation). 
By transforming the async method into a state machine, the compiler enables the method to be executed asynchronously while maintaining the logical flow of the original code. This allows developers to write asynchronous code that looks and behaves similarly to synchronous code, greatly simplifying the process of writing non-blocking, scalable applications [4](https://devblogs.microsoft.com/dotnet/how-async-await-really-works/)