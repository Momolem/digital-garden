
## Multiple Worktrees instead of stash
```sh
git worktree add ../example-prs develop
```
This creates a folder "example-prs" besides the current working directory and does a checkout on the branch develop.

This can be used for pull requests or hotfixes, it does not require to stash changes and does not need a rebuild of the project which does fix some issues.