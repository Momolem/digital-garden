---
share:
---
Units
## Game Of Life
- Iterativ
- OOP
- Git Basics (init, commit)

## Maui: Tic Tac Toe
- Konzepte: [[Model-View-Controller (MVC)|MVC]], [[Model-View-ViewModel (MVVM)|MVVM]], Bindings, Trennung Logik und UI

## Sortieralgorithmen
- Bubble Sort
- Selection Sort
- Insertion Sort
- Quicksort

Konzepte:
- Komplexitäten
- Arrayoperationen und schleifen
- Rekursion
## Uni Verwaltungsystem mit Azure DevOps
Prozess von vorne bis hinten erklären:
Scrum Prozess, PBI, Feature, Epic

1. User Stories definieren
2. Gruppieren in Features, Epics
3. Entity Relations modellieren
4. Domain Klassenmodell draften