---
share: true
aliases:
  - Red Grade
tags:
  - cleancode
title: Roter Grad
weigth: "1"
---

# Roter Grad
## Prinzipien
- [[DRY]]
- [[KISS]]
- [[Beware of Premature Optimization]]
- [[Favour Composition over Inheritance]]
- [[Integration Operation Segregation Principle (IOSP)]]

## Praktiken
- [[Boy Scout Rule]]
- [[Root Cause Analysis]]
- [[Version Control System]]
- [[Simple Refactorings]]
- [[Daily Reflection]]