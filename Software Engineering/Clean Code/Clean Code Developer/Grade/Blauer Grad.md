---
share: true
aliases:
  - Blue Grade
tags:
  - cleancode
title: Blauer Grad
weigth: "5"
---
# Blauer Grad
## Prinzipien
- [[Design and Implementation do not Overlapp]]
- [[Implementation Reflects Design]]
- [[YAGNI]]

## Praktiken
- [[Design before Implementation]]
- [[Continuous Delivery]]
- [[Iterative Development]]
- [[Incremental Development]]
- [[Component Orientation]]
- [[Test First]]