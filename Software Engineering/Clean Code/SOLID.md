---
aliases: 
share: true
tags: [cleancode]
title: SOLID
---
 
# SOLID
The SOLID Principles are a part of the [[Clean Code]] Principles. The letters stand for:
- [[Single Responsibility Principle]]
- [[Open Closed Principle]]
- [[Liskov Substitution Principle]]
- [[Interface Segragation Principle]]
- [[Dependency Inversion Principle]]
