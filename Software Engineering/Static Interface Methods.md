```csharp
public interface IId
{
	long Value { get; }
}

public interface IId<out Tid> : IId
{
	static abstract TId From(long rawId);
}

public class ActionId(long value) : IId<ActionId>
{
	public long Value { get; } = value;
	public static ActionId From(long rawId) => new(rawId);
}


public class TargetId(long value) : IId<TargetId>
{
	public long Value { get; } = value;
	public static TargetId From(long rawId) => new(rawId);
}

public static class MyExtensions
{

}

``` 