---
aliases: 
share: true
tags:
  - cleancode
title: Orangener Grad
weigth: "2"
---
 
# Orangener Grad
## Prinzipien
- [[Single Level of Abstraction]]
- [[Single Responsibility Principle]]
- [[Separation of Concerns]]
- [[Source Code Conventions]]

## Praktiken
- [[Issue Tracking]]
- [[Automated Integrationtests]]
- [[Read, Read, Read]]
- [[Code Reviews]]