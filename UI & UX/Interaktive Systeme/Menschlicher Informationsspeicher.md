## Informationstheorie von Shannon
![[UI & UX/Interaktive Systeme/assets/cc54222f2795eeed5330828d0c5e8403_MD5.png]]

### Sensorisches Gedächnis
- hohe Kapazität
- speichert sensorische Informationen
	- Wellenlänge
	- Töne
	- visuelle Reize (Formen und Farben)
- verliert Inhalt sehr schnell
	- visuelle Inhalte (ca. 0,2 Sekunden)
	- auditive INhalte (ca 1,5 Sekunden)
- Auffrischen nur durch Wiederholung möglich
	- wiederholtes Betrachten
	- Nachsprechen

### Arbeitsgedächnis
- stark begrenzte Kapazität
- speichert überwiegend symbolische Daten
- Vorhaltezeit
	- 15 Sekunden bei drei Einheiten
	- 130 Sekunden bei einer Einheit
	- durch Wiederholung ist längere Speicherung möglich
	- durch Überlagerung mit anderen Informationen (Interferenzeffekte) können Daten bereits nach kurzer Zeit nicht mehr behalten werden
- Aktuelle Untersuchungen widerlegen die Existenz eins Kurzzeitgedächtnisses
- Arbeitsgedächtnis vermutlich ein Teil des Langzeitgedächtnisses
(vgl. Festplatte mit SSD)

### Langzeitgedächnis
- nahezu unbegrenzter Speicher
	- bezogen auf Kapazität und Dauer
- enthält
	- episodische (=Bezug auf Raum und Zeit)
	- und semantische INformationen
- Zugriffszeit langsam
	- mind. 0,1 Sekunden
	- evtl. erheblich länger

### Recognize-Act-Zyklen
- Informationen werden wahrgenommen und kurzzeitig im sensorischen Speicher vorgehalten
- Die Musterkennung vergleicht Merkmale der wahrgenommenen Reize mit Informationen im  Langzeitgedächtnis (to recognize)
- Handlung erfolgt durch Interaktion zwischen Arbeitsgedächtnis und Langzeitgedächtnis (to act)
- Arbeitsgedächtnis repräsentiert die Ziele und Erwartung des Benutzers
- Im Erkennungsprozess nimmt man vorrangig wahr, wonach man aktuell sucht

## Arbeitsgedächnis
### Superzeichenbildung (Chunking)
- Superzeichen sind hierarchisch organisiert
- Beispiel Memory
	- Erwachsene nutzen Strategien (dritte Reihe von oben, zweite Karte von links)
	- Kinder merken sich vermutlich die Konstellation
	- Ergebnis – Kinder gewinnen in den meisten Fällen

- Beispiele für Superzeichenbildung
	- Wörter anstelle Einzelbuchstaben
	- Bilder (Eindrücke) anstelle des Wortes
	- Sätze anstelle Einzelworte
	- Funktioniert jedoch nur wenn die Informationseinheiten bekannt sind
- Symbolische Namen
	- 6A B7 anstelle 0110 1010 1011 0111
	- bluehands.de anstelle 109.109.201.186
### Konsequenzen
- Arbeitsgedächnis
	- kann nur wenige Einheiten halten (früher 7, heute 3-4)
	- Deshalb Superzeichenbildung (Abstrahierung)
	- ca. 15 Sekunden vorhaltezeit
	- Funktionsweise ähnlich wie Stack
- Konsequenzen:
	- Geschickte Gruppierung ist wichtig
	- Superzeichenbildung unterstützten, die dem Benutzer bekannt sind
	- Wenig, keine Ablenkung von der eigentlichen Aufgabe
	- Keine Verschachtelungen in Aufgaben (Stack nicht überfrachten)
	- Interferenzeffekte vermeiden


## Langzeitgedächnis
(Foliensatz 2)