---
share: true
aliases:
  - Green Grade
tags:
  - cleancode
title: Grüner Grad
weigth: "4"
---

# Grüner Grad
## Prinzipien
- [[Open Closed Principle]]
- [[Tell do not ask]]
- [[Law of Demeter]]

## Praktiken
- [[Continuous Integration]]
- [[Statical Code Analysis]]
- [[Inversion of Control Container]]
- [[Share Experience]]
- [[Error Measurement]]