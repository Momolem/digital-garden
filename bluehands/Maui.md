---
share:
---
https://learn.microsoft.com/de-de/training/paths/build-apps-with-dotnet-maui/?WT.mc_id=dotnet-35129-website

Explain MVC and MVVM

![[Model-View-Controller (MVC)]]

![[Model-View-ViewModel (MVVM)]]
