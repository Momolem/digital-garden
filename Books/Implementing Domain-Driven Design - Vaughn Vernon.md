---
title: Implementing Domain-Driven Design
author: Vaughn Vernon
category: Computers
publisher: Addison-Wesley
publishdate: 2013-02-06
pages: 656
cover: http://books.google.com/books/content?id=X7DpD5g3VP8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api
rating:
date read: 
status: to read
tags: book
aliases: [Implementing Domain-Driven Design (Book)]
linter-yaml-title-alias: Implementing Domain-Driven Design
---

# Implementing Domain-Driven Design (Book)