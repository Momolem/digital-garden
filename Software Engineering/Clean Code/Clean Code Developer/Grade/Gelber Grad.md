---
share: true
aliases:
  - Yellow Grade
tags:
  - cleancode
title: Gelber Grad
weight: "3"
---
 
# Gelber Grad
## Prinzipien
- [[Interface Segragation Principle]]
- [[Dependency Inversion Principle]]
- [[Liskov Substitution Principle]]
- [[Principle of Least Astonishment]]
- [[Information Hiding Principle]]

## Praktiken
- [[Automated Unit Tests]]
- [[Mockups]]
- [[Code Coverage Analysis]]
- [[Partizipation in Professional Events]]
- [[Complex Refactorings]]