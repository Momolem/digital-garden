Run ansible playbook

```shell
minikube start --driver=podman --container-runtime=containerd
```

Reset installation:

```shell
minikube stop
minikube delete --all
rm -rf ~/.minikube
```

More info:
https://minikube.sigs.k8s.io/docs/drivers/podman/

