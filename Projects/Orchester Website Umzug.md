Anbieter vergleiche:

| Anbieter  | Produkt            | Preis/m | Preis/a  | Traffic   | Mail             | Speicherplatz | Modalitäten                                    |
| --------- | ------------------ | ------- | -------- | --------- | ---------------- | ------------- | ---------------------------------------------- |
| Hetzner   | Webhosting Level 1 | 2.09 €  | 25.08 €  | unlimited | inkl. 100 Kunden | 10 GB         | Keine Mindestlaufzeit, 30 Tage kündigungsrecht |
| Hetzner   | Webhosting Level 4 | 5.39 €  | 64.68 €  | unlimited | inkl. 100 Kunden | 50 GB         | Keine Mindestlaufzeit, 30 Tage kündigungsrecht |
| Strato    | Wordpress Plus     | 9.00 €  | 108.00 € | unlimited | 3 Postfächer     | 50 GB         |                                                |
| Strato    | Wordpress Basic    | 5.00 €  | 60.00 €  | unlimited | 1 Postfach       | 25 GB         |                                                |
| Netcup    | Webhosting 1000    | 2.17 €  | 26.04 €  | unlimited | 100 Postfächer   | 25 GB         |                                                |
| Netcup    | Webhosting 2000    | 3.25 €  | 39.00 €  | unlimited | 500 Postfächer   | 75 GB         |                                                |
| Netcup    | Webhosting 4000    | 6.52 €  | 78.24 €  | unlimited | 1000 Postfächer  | 250 GB        |                                                |
| Wordpress |                    | 25.00 € | 300.00 € |           |                  |               |                                                |
| raidboxes | Mini               | 15.00 € | 180.00 € |           |                  |               |                                                |


# Domain umzug
-> Authcode

https://help.jimdo.com/hc/de/articles/115005537746-Wo-finde-ich-den-Auth-Code-f%C3%BCr-meine-Domain


## Offene Fragen
Content Umzug
Verzögertes umziehen von domain? 