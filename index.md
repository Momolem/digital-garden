---
aliases: 
tags:
  - DigitalGarden
share: true
---

# Momolems Digital Garden
This is where my knowledge grows

This far I managed to include some notes on the topics CleanCode, UI & UX and Cooking
I want to expand these topics with everything I know

## Clean Code
[[Clean Code|CleanCode]]
- [[Clean Code Developer]]
- [[Design Patterns]]

## UI & UX
[[Design Patterns|Entwurfsmuster]]

## Cooking
[[Baghali Polo]]