---
title: Refactoring
author: Martin Fowler
category: Computers
publisher: MITP-Verlags GmbH & Co. KG
publishdate: 2020-03-20
pages: 589
cover: http://books.google.com/books/content?id=n9TXDwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api
rating:
date read: 
status: to read
tags: book
aliases: [Refactoring (Book)]
linter-yaml-title-alias: Refactoring
---

# Refactoring (Book)